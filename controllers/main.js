const getTableData = (req, res, db) => {
  db.select('*').from('main')
    .then(items => {
      if(items.length){
        res.json(items)
      } else {
        res.json({dataExists: 'false'})
      }
    })
    .catch(err => {
      console.log(err)
      res.status(400).json({dbError: 'db error'})
    })
}

const postTableData = (req, res, db) => {
  const { first, last, email, phone, location, hobby } = req.body
  const added = new Date()
  db('main').insert({first, last, email, phone, location, hobby, added})
    .then(id => {
      db.select('*').from('main').where({id})
      .then(items => {
        if(items.length){
          res.json(items)
        } else {
          res.json({dataExists: 'false'})
        }
      })
    })
    .catch(err => {
      console.log(err)
      res.status(400).json({dbError: 'db error'})
    })
}

const putTableData = (req, res, db) => {
  const { id, first, last, email, phone, location, hobby } = req.body
  db('main').where({id}).update({first, last, email, phone, location, hobby})
    .then( () => {
      db('main').from('main').where({id})
      .then(items => {
        if(items.length){
          res.json(items)
        } else {
          res.json({dataExists: 'false'})
        }
      })
    })
    .catch(err => {
      console.log(err)
      res.status(400).json({dbError: 'db error'})
    })
}

const deleteTableData = (req, res, db) => {
  const { id } = req.body
  db('main').where({id}).del()
    .then(() => {
      res.json({delete: 'true'})
    })
    .catch(err => {
      console.log(err)
      res.status(400).json({dbError: 'db error'})
    })
}

module.exports = {
  getTableData,
  postTableData,
  putTableData,
  deleteTableData
}
