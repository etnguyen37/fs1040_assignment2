# Backend API 

This project utilizes NodeJs and MySQL to provide a backend service for the project that is being utilized.

## Running API
In order to run the API, be sure to run `npm install`
Once done, simply run `npm run start`, which will setup the database tables, as well as start the node server.

**Note** The project utilizes environment variables for configuration settings found in `knexfile.js`

## Running Tests
To run tests, run `npm run test`, this will setup the database tables, and run the mocha test runner. 

**Note** The project expects a MySQL database. A `.env` can also be used. See `.env.example` file for layout, you may rename this file and update as needed. 
